package plugins.stef.tools;

import java.util.ArrayList;
import java.util.List;

import icy.gui.dialog.MessageDialog;
import icy.gui.frame.progress.FailedAnnounceFrame;
import icy.roi.BooleanMask3D;
import icy.roi.ROI;
import icy.sequence.Sequence;
import icy.swimmingPool.SwimmingObject;
import icy.type.collection.CollectionUtil;
import icy.type.point.Point5D;
import icy.type.rectangle.Rectangle5D;
import plugins.adufour.blocks.lang.Block;
import plugins.adufour.blocks.util.VarList;
import plugins.adufour.ezplug.EzPlug;
import plugins.adufour.ezplug.EzVarBoolean;
import plugins.adufour.ezplug.EzVarSwimmingObject;
import plugins.adufour.vars.lang.Var;
import plugins.adufour.vars.lang.VarROIArray;
import plugins.kernel.roi.roi2d.ROI2DArea;
import plugins.kernel.roi.roi2d.ROI2DPoint;
import plugins.kernel.roi.roi3d.ROI3DArea;
import plugins.nchenouard.spot.DetectionResult;
import plugins.nchenouard.spot.Point3D;
import plugins.nchenouard.spot.Spot;

/**
 * This plugin / block allow to convert a DetectionResult object containing a set of Detection into a set of ROI.
 * 
 * @author Stephane
 */
public class DetectionToROI extends EzPlug implements Block
{
    protected final EzVarSwimmingObject<DetectionResult> swimmingObject;
    protected final Var<DetectionResult> varDetectionResult;
    protected final VarROIArray varRois;
    protected final EzVarBoolean removePrevRois;

    public DetectionToROI()
    {
        super();

        swimmingObject = new EzVarSwimmingObject<DetectionResult>("Detection result");
        varDetectionResult = new Var<DetectionResult>("Detection result", DetectionResult.class, null, null);
        varRois = new VarROIArray("ROI(s)");
        removePrevRois = new EzVarBoolean("Remove previous ROI(s)", false);
    }

    @Override
    public void declareInput(VarList inputMap)
    {
        inputMap.add("detectionResult", varDetectionResult);
    }

    @Override
    public void declareOutput(VarList outputMap)
    {
        outputMap.add("roi(s)", varRois);
    }

    @Override
    protected void initialize()
    {
        addEzComponent(swimmingObject);
        addEzComponent(removePrevRois);
    }

    @Override
    public void clean()
    {
        //
    }

    @Override
    protected void execute()
    {
        final Sequence sequence;
        DetectionResult detectionResult;

        // headless / protocol mode
        if (isHeadLess())
        {
            // no output sequence
            sequence = null;
            // get from protocol variable
            detectionResult = varDetectionResult.getValue();
        }
        // interactive mode
        else
        {
            // get object from swimming pool
            final SwimmingObject obj = swimmingObject.getValue();
            // output sequence
            sequence = getActiveSequence();

            if (obj != null)
            {
                // no output sequence in interactive mode ?
                if ((sequence == null) && !isHeadLess())
                {
                    // nothing to do
                    detectionResult = null;
                    // display a message
                    MessageDialog.showDialog("You need to have an opened Sequence to receive result as ROI(s)",
                        MessageDialog.INFORMATION_MESSAGE);
                }
                // get result
                else detectionResult = (DetectionResult) obj.getObject();
            }
            else detectionResult = null;
        }

        final ROI[] rois;

        try
        {
            // nothing to do --> set empty result
            if ((detectionResult == null) || (detectionResult.getNumberOfDetection() == 0)) rois = new ROI[0];
            else
                // do conversion
                rois = convertToRois(detectionResult);

            // set result
            varRois.setValue(rois);

            if ((rois.length > 0) && (sequence != null))
            {
                sequence.beginUpdate();
                try
                {
                    if (removePrevRois.getValue().booleanValue()) sequence.removeAllROI();
                    sequence.addROIs(CollectionUtil.asList(rois), false);
                }
                finally
                {
                    sequence.endUpdate();
                }
            }
        }
        catch (InterruptedException e)
        {
            new FailedAnnounceFrame("Detection to ROI process interrupted.");
        }
    }

    /**
     * Convert the set of ROI into a DetectionResult object containing a set of Detection.
     * 
     * @param detectionResult
     *        DetectionResult to convert to a set of ROI
     * @throws InterruptedException
     */
    public static ROI[] convertToRois(DetectionResult detectionResult) throws InterruptedException
    {
        final List<ROI> result = new ArrayList<ROI>();
        final int minT = detectionResult.getFirstFrameTime();
        final int maxT = detectionResult.getLastFrameTime();

        for (int t = minT; t <= maxT; t++)
        {
            for (Spot spot : detectionResult.getDetectionsAtT(t))
            {
                final List<Point3D> pts3d = spot.point3DList;
                final Point3D pos = spot.mass_center;
                ROI roi;

                // no points position
                if (pts3d.isEmpty())
                {
                    // create Point ROI from mass center
                    roi = new ROI2DPoint(pos.x, pos.y);
                    // set Z position
                    ((ROI2DPoint) roi).setZ((int) pos.z);
                }
                else
                {
                    final List<icy.type.point.Point3D> icyPts3d = new ArrayList<icy.type.point.Point3D>();

                    // convert to Icy point3D
                    for (Point3D pt3d : pts3d)
                        icyPts3d.add(new icy.type.point.Point3D.Double(pt3d.x, pt3d.y, pt3d.z));

                    // build the 3D mask
                    final BooleanMask3D mask3d = new BooleanMask3D(
                        icyPts3d.toArray(new icy.type.point.Point3D.Double[icyPts3d.size()]));
                    // build the 3D roi from it
                    final ROI3DArea roi3d = new ROI3DArea(mask3d);
                    final Rectangle5D bounds = roi3d.getBounds5D();

                    // single Z dimension
                    if (bounds.isInfiniteZ() || (bounds.getSizeZ() == 1d))
                    {
                        if (bounds.isInfiniteZ())
                        {
                            roi = new ROI2DArea(roi3d.getBooleanMask2D(0, true));
                            // set position to all Z
                            ((ROI2DArea) roi).setZ(-1);
                        }
                        else
                        {
                            final int z = (int) bounds.getZ();
                            roi = new ROI2DArea(roi3d.getBooleanMask2D(z, true));
                            // set position to correct Z
                            ((ROI2DArea) roi).setZ(z);
                        }
                    }
                    else roi = roi3d;
                }

                // get ROI position
                final Point5D roiPos = roi.getPosition5D();
                // set current T position
                roiPos.setT(t);

                // add to result list
                result.add(roi);
            }
        }

        // set name for ROI
        final Integer numberSize = Integer.valueOf((int) Math.log10(result.size()) + 1);
        int ind = 0;

        for (ROI roi : result)
            roi.setName("Detection #" + String.format(String.format("%%0%dd", numberSize), Integer.valueOf(ind++)));

        return result.toArray(new ROI[result.size()]);
    }
}
